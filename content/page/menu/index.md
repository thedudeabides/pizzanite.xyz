---
title: Pizza menu
subtitle: One pizza is never enough
url: /menu/
---

Όλα τα πιτσόνια φτιάχνονται με ολόφρεσκα υλικά.

## Psistis suggestions

Με κόκκινη σάλσα:

- **pizza apola**: ζαμπό, τυρί, μπέικο, πιπεριά, μανιτάρι, τομάτα
- **pizza village**: φέτα, ελιά, κρεμμύδι, τομάτα, πιπεριά
- **pizza loukanik**: πιπεριά, λουκάνικο, τυρί
- **pizza roka**: μοτσαρέλα, προσούτο, ρόκα
- **pizza ananas**: ζαμπό, τυρί, ανανάς

Με λευκή σάλσα:

- **pizza vromeri**: μοτσαρέλα, ροκφόρ, μανιτάρι, προσούτο, ρόκα
- **pizza kari**: κοτόπουλο, κρεμμύδι, ανανάς

## Make your own

Τα υλικά θα είναι στη διάθεση του καθενός για να δημιουργήσει την δικιά του pizza.
